/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function() {
	// Default starting schema
	// (not bothering with url saves)
	var schema = {};
	
	// Divs/textareas on the page
	var $schema = document.getElementById('schema');
	var $output = document.getElementById('output');
	var $editor = document.getElementById('editor');
	var $validate = document.getElementById('validate');

	// Buttons
	var $set_schema_button = document.getElementById('setschema');
	var $set_value_button = document.getElementById('setvalue');
	
	var jsoneditor;
	
	var reload = function(keep_value) {
		var startval = (jsoneditor && keep_value) ?
					jsoneditor.getValue() : window.startval;
		window.startval = undefined;

		if(jsoneditor) jsoneditor.destroy();
			jsoneditor = new JSONEditor($editor,{
				schema: schema,
				startval: startval
		});
		window.jsoneditor = jsoneditor;

		// When the value of the editor changes, update the JSON output and validation message
		jsoneditor.on('change',function() {
			
			// id setter
			var i = 0;
			while(jsoneditor.getEditor('root.'+ i) !== undefined ) {
				var record = jsoneditor.getEditor(
							'root.'+i+'.id');
				record.setValue(i);
			i++;}
			
			var json = jsoneditor.getValue();

			$output.value = JSON.stringify(json,null,2);

			var validation_errors = jsoneditor.validate();
			// Show validation errors if there are any
			if(validation_errors.length) {
				$validate.value = JSON.stringify(validation_errors,null,2);
			}
			else {
				$validate.value = 'valid';
			}

		});
	};
	
	// Start the schema and output textareas with initial values
	$schema.value = JSON.stringify(schema,null,2);
	$output.value = '';
	
	// When the 'update form' button is clicked, set the editor's value
    $set_value_button.addEventListener('click',function() {
        jsoneditor.setValue(JSON.parse($output.value));
    });
		
	// Update the schema when the button is clicked
	$set_schema_button.addEventListener('click',function() {
		try {
			schema = JSON.parse($schema.value);
		}
		catch(e) {
			alert('Invalid Schema: '+e.message);
			return;
		}

		reload();
	});
	
	JSONEditor.defaults.options.theme = "bootstrap3";
	JSONEditor.defaults.options.iconlib = "bootstrap3";
	
	var refreshBooleanOptions = function(no_reload) {
		var boolean_options =
					document.getElementById('boolean_options').children;
		
		for(var i=0; i<boolean_options.length; i++) {
			JSONEditor.defaults.options[boolean_options[i].value] =
						boolean_options[i].selected;
		}
		if(!no_reload) reload(true);
	};
	
	// Change listeners for options
	document.getElementById('object_layout').addEventListener(
		'change',function() {
		 JSONEditor.defaults.options.object_layout = this.value;
			reload(true);
	});
	document.getElementById('show_errors').addEventListener(
		'change',function() {
		 JSONEditor.defaults.options.show_errors = this.value;
			reload(true);
	});
	document.getElementById('boolean_options').addEventListener(
		'change',function() {
			refreshBooleanOptions();
	});
	
	reload();
})();